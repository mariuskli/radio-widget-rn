export const data = [
  {
    id: 1,
    name: "Putin FM",
    freq: "66,6"
  },
  {
    id: 2,
    name: "Dribbble FM",
    freq: "101,2"
  },
  {
    id: 3,
    name: "Doge FM",
    freq: "99,4"
  },
  {
    id: 4,
    name: "Ballads FM",
    freq: "87,1"
  },
  {
    id: 5,
    name: "Maximum FM",
    freq: "142,2"
  }
];