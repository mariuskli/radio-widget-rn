import React, { useState } from 'react';
import styled from 'styled-components/native';
import Navbar from './components/NavbarC';
import Footer from './components/FooterC';
import Stations from './components/StationsC';
import { data } from './data';

const App = () => {
  const [activeStation, setActiveStation] = useState(null);

  const onStationPress = (index) => {
    index === activeStation ? setActiveStation(null) : setActiveStation(index);
  };

  return (
    <Container>
      <Navbar />
      <Stations data={data} onPress={onStationPress} activeStation={activeStation}/>
      <Footer activeStation={activeStation !== null ? data[activeStation] : null} />
    </Container>
  );
};

const Container = styled.View`
  flex: 1;
  flex-direction: column;
  background-color: #272732;
`; 

export default App;
