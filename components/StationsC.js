import React from 'react';
import styled from 'styled-components/native';
import PropTypes from 'prop-types';
import { FlatList } from 'react-native';
import Station from './StationC';


const Stations = ({data, onPress, activeStation}) => {
  const renderItem = ({item, index}) => <Station station={item} index={index} onPress={onPress} isActive={activeStation === index ? true : false}/>;

  return (
    <Container>
      <FlatList
        data={data}
        renderItem={renderItem}
        extraData={activeStation}
        ItemSeparatorComponent={Separator}
        keyExtractor={item => item.id.toString()}
        showsHorizontalScrollIndicator={false}
      />
    </Container>
  );
};

const Separator = styled.View`
  height: 2px;
  width: 100%;
  backgroundColor: #3d3d46;
`;

const Container = styled.View`
  flex: 1;
  padding: 18px 15px;
`;

Stations.propTypes = {
  data: PropTypes.array.isRequired,
  onPress: PropTypes.func.isRequired,
  activeStation: PropTypes.number
};

export default Stations;