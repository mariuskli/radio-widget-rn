import React from 'react';
import styled from 'styled-components/native';
import { TouchableOpacity, Image, StyleSheet } from 'react-native';
import Constants from 'expo-constants'

const Navbar = () => {
  return (
    <Container>
      <TouchableOpacity>
        <Image source={require('../assets/back-arrow.png')} style={styles.leftImage}/>
      </TouchableOpacity>
      <Text>STATIONS</Text>
      <TouchableOpacity>
        <Image source={require('../assets/switch.png')} style={styles.rightImage} />
      </TouchableOpacity>
    </Container>
  );
}

const styles = StyleSheet.create({
  leftImage: {
    width: 14,
    height: 24,
    resizeMode: 'cover'
  },
  rightImage: {
    width: 22,
    height: 26,
    resizeMode: 'cover'
  }
});

const Container = styled.View`
  align-items: center;
  justify-content: space-between;
  background-color: #eaa455;
  color: #fff;
  flex-direction: row;
  padding: ${(Constants.statusBarHeight + 20)}px 15px 20px;
`;

const Text = styled.Text`
  font-size: 24px;
  font-weight: 700;
  color: #ffffff;
`;

export default Navbar;