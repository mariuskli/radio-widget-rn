import React from 'react';
import styled from 'styled-components/native';
import PropTypes from 'prop-types';
import { View } from 'react-native';

const Footer = ({activeStation}) => {
  return (
    <Container>
      { activeStation &&
        <View>
          <SmallText>CURRENTLY PLAYING</SmallText>
          <LargeText>{activeStation.name}</LargeText>
        </View>
      }
    </Container>
  );
};

const SmallText = styled.Text`
  text-align: center;
  color: #edae61;
  font-size: 12px;
  text-transform: uppercase;
`;

const LargeText = styled.Text`
  text-align: center;
  color: #a2abbd;
  font-size: 24px;
`;

const Container = styled.View`
  height: 100px;
  justify-content: center;
  align-items: center;
  background-color: #22222b;
  borderTopWidth: 2px;
  borderTopColor: #515561;
  borderStyle: solid;
`;

Footer.propTypes = {
  activeStation: PropTypes.shape({
    name: PropTypes.string.isRequired,
    freq: PropTypes.string.isRequired
  })
};

export default Footer;