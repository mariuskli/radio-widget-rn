import React, { useState } from 'react';
import styled from 'styled-components/native';
import { Image } from 'react-native';

const LoadableImage = (props) => {

  const [isLoading, setIsLoading] = useState(false);

  return (
    <Container>
      <Image 
        {...props}
        onLoadStart={() => setIsLoading(true)}
        onLoadEnd={() => setIsLoading(false)}
      />
      { isLoading && <Spinner animated={true} /> }
    </Container>
  );
};

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const Spinner = styled.ActivityIndicator`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
`;

export default LoadableImage;