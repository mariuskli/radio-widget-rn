import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { TouchableOpacity, View, StyleSheet } from 'react-native';
import { Transition } from 'react-spring/renderprops';
import LoadableImage from './LoadableImageC';

const Station = ({station, onPress, isActive, index}) => {
  const { name, freq } = station;
  return (
    <View>
      <Transition config={{duration: 250}} items={isActive} from={{height: 0, opacity: 0}} enter={{height: 200, opacity: 1}} leave={{height: 0, opacity: 0}}>
      {isActive => 
        isActive && (
          props => (
            <Panel style={props}>
              <TouchableOpacity>
                <ButtonImage source={require('../assets/minus.png')} style={{marginRight: 15}} />
              </TouchableOpacity>
              <LoadableImage source={require('../assets/station-image.png')} style={styles.thumb}/>
              <TouchableOpacity>
                <ButtonImage source={require('../assets/plus.png')} style={{marginLeft: 15}} />
              </TouchableOpacity>
            </Panel>
          )
        )
      }
      </Transition>
      <TouchableOpacity onPress={() => onPress(index)}>
        <Container>
          <Text>{name}</Text>
          <Text>{freq}</Text>
        </Container>
      </TouchableOpacity>
    </View>
  );
};

const ButtonImage = styled.Image`
  width: 40;
  height: 40;
  resizeMode: cover;
`;

const styles = StyleSheet.create({
  thumb: {
    resizeMode: "cover",
    width: 160,
    height: 160,
    borderRadius: 80,
    borderWidth: 3,
    borderColor: "#a2abbd"
  }
});

const Panel = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`;

const Container = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const Text = styled.Text`
  padding: 18px 0;
  color: #a2abbd;
  font-size: 24px;
`;

Station.propTypes = {
  station: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    freq: PropTypes.string
  }).isRequired,
  onPress: PropTypes.func.isRequired,
  isActive: PropTypes.bool.isRequired
};

export default Station;